// spec.js
describe('Protractor Demo App', function() {
  it('should create a user', function() {
    browser.get('http://54.169.123.170:32809/users');
    element(by.css('a[href="/users/new"]')).click();

    var form = element(by.id('new_user'));
    var first_name = form.element(by.name('user[first_name]'))
    var last_name = form.element(by.name('user[last_name]'))
    var gender = form.element(by.name('user[gender]'))

    first_name.sendKeys("hello");
    last_name.sendKeys("kitty");
    gender.$('[value="others"]').click();
    form.element(by.name('commit')).click();
    browser.sleep(2000);
    expect(element(by.css('tbody > tr > th:nth-child(1)')).getText()).toEqual('hello');
    expect(element(by.css('tbody > tr > th:nth-child(2)')).getText()).toEqual('kitty');
    expect(element(by.css('tbody > tr > th:nth-child(4)')).getText()).toEqual('others');
  });

  it('should edit info. of user', function() {
  	browser.get('http://54.169.123.170:32809/users');
  	element(by.css('tbody tr:nth-child(1) td:nth-child(9) a')).click();
    browser.waitForAngular();

  	var user = element(by.css('form'));
  	user.element(by.name('first')).clear().sendKeys("nana");
  	user.element(by.name('last')).clear().sendKeys("mizuki");
    user.element(by.name('age')).clear().sendKeys("17");

  	user.element(by.css('select > option:nth-child(2)')).click();
  	user.element(by.name('country')).clear().sendKeys("JP");
  	user.element(by.name('address_1')).clear().sendKeys("tokyo");
  	user.element(by.name('address_2')).clear().sendKeys("tw");
  	element(by.css('[ng-click="update()"]')).click();
    browser.sleep(2000)
    expect(element(by.css('tbody > tr > th:nth-child(1)')).getText()).toEqual('nana');
    expect(element(by.css('tbody > tr > th:nth-child(2)')).getText()).toEqual('mizuki');
    expect(element(by.css('tbody > tr > th:nth-child(3)')).getText()).toEqual('17');
    expect(element(by.css('tbody > tr > th:nth-child(4)')).getText()).toEqual('female');
    expect(element(by.css('tbody > tr > th:nth-child(5)')).getText()).toEqual('JP');
    expect(element(by.css('tbody > tr > th:nth-child(6)')).getText()).toEqual('tokyo');
    expect(element(by.css('tbody > tr > th:nth-child(7)')).getText()).toEqual('tw');
  });

  it('should delete created user', function() {
    browser.get('http://54.169.123.170:32809/users');
    element(by.css('tr:nth-child(1) td:nth-child(10) > a')).click();
    browser.switchTo().alert().accept();
    expect(element(by.css('#notice')).getText()).toEqual('User was successfully destroyed.');
  });

  it('should reduce user count', function() {
    browser.get('http://54.169.123.170:32809/users');
    origin_count = element(by.css('tbody tr')).length
    if (origin_count > 0) {
      sleep(10000)
      element(by.css('tr:nth-child(1) td:nth-child(10) > a')).click();
      sleep(10000)
      browser.switchTo().alert().accept();
      sleep(10000)
      count = element(by.css('tbody tr')).length
      sleep(10000)
      diff = origin_count - count
    }else
      diff = 1
    expect(diff).toEqual(1);
  });

});
