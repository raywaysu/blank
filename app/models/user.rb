class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Enum

  field :first_name, type: String
  field :last_name, type: String
  field :age, type: Integer
  field :address, type: Hash, default: { country: '', address_1: '', address_2: '' }


  validates_presence_of :first_name, :last_name
	validates :age, numericality: { greater_than: 0 }, allow_nil: true

  enum :gender, [:male, :female, :others], :default => ""


  has_one :shop

  alias_method :origin_gender, :gender
  alias_method :origin_id, :id

  def name
  	"#{first_name} #{last_name}"
  end

  def gender
  	origin_gender.to_s
  end

  def id
    origin_id.to_s
  end
end
