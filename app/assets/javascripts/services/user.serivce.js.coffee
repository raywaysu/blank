@app.service('userService', ($http, $q) ->
	{
	  getUser: (id) ->
	  	$http.get('/users/' + id + '.json')
		  	.then (resp) ->
				  if typeof resp.data == 'object'
				    resp.data
				  else
				    $q.reject resp.data
				, (resp) ->
				  $q.reject resp.data

		updateUser: (user) ->
			$http.patch('/users/' + user.id, JSON.stringify(user: user))
				.then (resp) ->
					if resp.data.result == 'success'
						window.location = "/users/" + user.id
					else
						$q.reject resp.data
				, (resp) ->
				  $q.reject resp.data
	}
)