@app.directive 'userForm', [ () ->
  {
    templateUrl: '/templates/user_form.html'
    restrict: 'AE'
    controller: 'UsersEditController'
  }
]
