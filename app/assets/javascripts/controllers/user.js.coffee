@app.controller('UsersController', ['$scope', '$http', 'userService', ($scope, $http, userService) ->
	id = window.location.pathname.slice(7)

	userService.getUser(id)
		.then (data) ->
			$scope.user = data
		, (error) ->
		  console.log error
])
