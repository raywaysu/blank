@app.controller('UsersEditController', ['$scope', '$http', 'userService', ($scope, $http, userService) ->
	id = window.location.pathname.slice(7)
	id = id.substring(0, id.indexOf('/'))
	$scope.genders = ["male", "female", "others"]

	userService.getUser(id)
		.then (data) ->
			$scope.user = data
		, (error) ->
		  console.log error

	$scope.update = () ->
		userService.updateUser($scope.user)
			.then (data) ->
				console.log data
			, (error) ->
				console.log error
])
